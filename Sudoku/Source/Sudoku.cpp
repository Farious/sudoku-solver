//******************************************************************************
// Sudoku Table Class 
//
// Author: Fabio R.   Date: 21/09/2013
//******************************************************************************
#include "Sudoku.hpp"

////////////////////////////////////////////////////////////////////////////////
// Sudoku table access
//
int* sudoku::getCell(int i, int j){
	return table + (i * 9 * 9 + j * 9);
};

int sudoku::getCellVal(int i, int j){
	int *cell = getCell(i, j);
	int result = 0, val = 0;

	for (int c = 0; c < 9; c++){
		int val = cell[c];
		if (val != 0 && result == 0)
			result = cell[c];
		else if (val != 0 && result != 0)
			return 0;
		else
			continue;
	};

	return result;
};

int* sudoku::getBlock(int i, int j){
	int *ret = new int[2];
	ret[0] = (i - i % 3) / 3;
	ret[1] = (j - j % 3) / 3;
	return ret;
};

int* sudoku::getBlockRange(int i, int j){
	int *range = new int[4];
	range[0] = i * 3;
	range[1] = i * 3 + 2;
	range[2] = j * 3;
	range[3] = j * 3 + 2;
	return range;
};

////////////////////////////////////////////////////////////////////////////////
// Sudoku Validation functions
//
// Verify block i,j
bool sudoku::verifyBlock(int i, int j){
	int *range = getBlockRange(i, j);
	int i_min = range[0], i_max = range[1],
		j_min = range[2], j_max = range[3];

	int availableValues[9];

	for (int c = 0; c < 9; c++)
		availableValues[c] = c + 1;

	int val;
	for (int cell_i = i_min; cell_i <= i_max; cell_i++)
		for (int cell_j = j_min; cell_j <= j_max; cell_j++)
		{
			val = getCellVal(cell_i, cell_j);

			if (val == 0)
				continue;

			int crnt = availableValues[val - 1];
			if (crnt == 0)
				return false;
			else
				availableValues[val - 1] = 0;
		}

	delete[] range;
	return true;
};

// Verify all blocks
bool sudoku::verifyAllBlocks(){
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			if (!verifyBlock(i, j))
				return false;

	return true;
};

// Verify line i
bool sudoku::verifyLine(int i){
	int availableValues[9];
	int val;

	for (int c = 0; c < 9; c++)
		availableValues[c] = c + 1;

	for (int j = 0; j < 9; j++){
		val = getCellVal(i, j);

		if (val == 0)
			continue;

		int crnt = availableValues[val - 1];
		if (crnt == 0)
			return false;
		else
			availableValues[val - 1] = 0;
	}

	return true;
};

// Verify all lines
bool sudoku::verifyAllLines(){
	for (int i = 0; i < 9; i++)
		if (!verifyLine(i))
			return false;
	return true;
};

// Verify row j
bool sudoku::verifyRow(int j){
	int availableValues[9];
	int val;

	for (int c = 0; c < 9; c++)
		availableValues[c] = c + 1;

	for (int i = 0; i < 9; i++){
		val = getCellVal(i, j);

		if (val == 0)
			continue;

		int crnt = availableValues[val - 1];
		if (crnt == 0)
			return false;
		else
			availableValues[val - 1] = 0;
	}

	return true;
};

// Verify all rows
bool sudoku::verifyAllRows(){
	for (int j = 0; j < 9; j++)
		if (!verifyRow(j))
			return false;
	return true;
};

// Verify a Cell
bool sudoku::verifyCell(int i, int j){
	int *values = getCell(i, j);
	int count = 0;

	for (int c = 0; c < 9; c++)
		count += values[c] > 0;

	if (count == 0)
		return false;

	return true;
};

// Verify all Cells
bool sudoku::verifylAllCells(){
	for (int i = 0; i < 9; i++)
		for (int j = 0; j < 9; j++)
			if (!verifyCell(i, j))
				return false;

	return true;
};

// Verify Sudoku
bool sudoku::verify(){
	bool result = true;

	result = verifylAllCells();
	if (!result)
		return false;

	result = verifyAllLines();
	if (!result)
		return false;

	result = verifyAllRows();
	if (!result)
		return false;

	result = verifyAllBlocks();
	return true;
}

// Verify if finished
bool sudoku::finished(){
	int count = 0;
	for (int i = 0; i < 9 * 9 * 9; i++)
		count += (table[i] > 0);

	return count == 9 * 9;
}

////////////////////////////////////////////////////////////////////////////////
// Input/Output
//
bool sudoku::parse(string game){
	int ni = 0, nj = 0;
	for (int i = 0; i < 9 * 9; i++){
		int icalc = i * 9;
		int val = game[ni * 9 + nj] - 48;
		if (val > 0){
			for (int j = 1; j <= 9; j++)
				table[icalc + j - 1] = 0;
			table[icalc + val - 1] = val;
		}

		nj++;
		if (nj == 9){
			nj = 0;
			ni++;
		}
	};

	return true;
};

void sudoku::printTable(){
	cout << "#########################" << endl;
	cout << "#                       #" << endl;
	for (int i = 0; i < 9; i++){
		if (i == 3 || i == 6)
			cout << "# --------------------- #" << endl;
		cout << "# ";
		for (int j = 0; j < 9; j++){
			if (j == 3 || j == 6)
				cout << "| ";
			cout << getCellVal(i, j) << " ";
		};
		cout << "#" << endl;
	}
	cout << "#                       #" << endl;
	cout << "#########################" << endl;
};

void sudoku::printTableTotal(){
	cout << "#############################################" << endl;
	cout << "#                                           #" << endl;
	for (int i = 0; i < 9; i++){
		if (i == 3 || i == 6)
			cout << "# ========================================= #" << endl;
		cout << "# ";
		for (int k = 0; k < 3; k++){
			if (k > 0)
				cout << "# ";
			for (int j = 0; j < 9; j++){
				int *values = getCell(i, j);
				if (j == 3 || j == 6)
					cout << " || ";
				else if (j > 0)
					cout << "|";
				for (int c = k * 3; c < (k + 1) * 3; c++){
					cout << values[c];
				}
			};
			cout << " #" << endl;
		};
		if (i != 2 && i != 5 && i != 8)
			cout << "#                                           #" << endl;
	}
	cout << "#                                           #" << endl;
	cout << "#############################################" << endl;
};

////////////////////////////////////////////////////////////////////////////////
// Setting/Checking cell properties
//
void sudoku::cleanLine(int i, int j_keep, int val){
	for (int j = 0; j < 9; j++){
		if (j == j_keep)
			continue;
		getCell(i, j)[val - 1] = 0;
	}
};

void sudoku::cleanRow(int i_keep, int j, int val){
	for (int i = 0; i < 9; i++){
		if (i == i_keep)
			continue;
		getCell(i, j)[val - 1] = 0;
	}
};

void sudoku::cleanBlock(int i_keep, int j_keep, int val){
	int *block = getBlock(i_keep, j_keep);
	int *range = getBlockRange(block[0], block[1]);
	int i_min = range[0], i_max = range[1],
		j_min = range[2], j_max = range[3];

	for (int i = i_min; i <= i_max; i++)
		for (int j = j_min; j <= j_max; j++){
			if (i == i_keep && j == j_keep)
				continue;

			getCell(i, j)[val - 1] = 0;
		}
};

bool sudoku::checkLine(int i, int val){
	for (int j = 0; j < 9; j++){
		int *cell = getCell(i, j);
		if (cell[val - 1] == 0)
			continue;
		else{
			int count = 0;
			for (int n = 0; n < 9; n++)
				count += cell[n] == 0;
			if (count == 8)
				return true;
		}
	}
	return false;
};

bool sudoku::checkLineOther(int i, int j_keep, int val){
	for (int j = 0; j < 9; j++){
		int *cell = getCell(i, j);
		if (j == j_keep)
			continue;
		if (cell[val - 1] == 0)
			continue;
		else{
			int count = 0;
			for (int n = 0; n < 9; n++)
				count += cell[n] == 0;
			if (count == 8)
				return true;
		}
	}
	return false;
};

void sudoku::setValue(int i, int j, int val){
	int *cell = getCell(i, j);
	for (int c = 0; c < 9; c++)
		if (c != val - 1)
			cell[c] = 0;

	cleanLine(i, j, val);
	cleanRow(i, j, val);
	cleanBlock(i, j, val);
};

int sudoku::countPossible(int i, int j){
	int *values = getCell(i, j);
	int count = 0;

	for (int c = 0; c < 9; c++)
		count += (values[c] != 0);

	return count;
};

////////////////////////////////////////////////////////////////////////////////
// Solving functions
//
bool sudoku::refresh(){
	bool ok = true;
	int count = 1;
	while (count > 0 && ok){
		count = updateCells();

		for (int i = 0; i < 9; i++){
			count += chkLine(i);
			count += chkRow(i);
			for (int j = 0; j < 9; j++)
				count += chkBlock(i, j);
		};

		ok = verify();
	};

	return ok;
}

// Check available values for a cell (in a Row)
int *sudoku::chkAvRow(int self_i, int self_j){
	int *availableVal = new int[9];
	for (int c = 0; c < 9; c++)
		availableVal[c] = c + 1;

	for (int j = 0; j < 9; j++){
		if (j == self_j)
			continue;

		int val = getCellVal(self_i, j);
		if (val == 0)
			continue;

		availableVal[val - 1] = 0;
	}

	return availableVal;
};

// Check available values for a cell (in a Collumn)
int *sudoku::chkAvCol(int self_i, int self_j){
	int *availableVal = new int[9];
	for (int c = 0; c < 9; c++)
		availableVal[c] = c + 1;

	for (int i = 0; i < 9; i++){
		if (i == self_i)
			continue;

		int val = getCellVal(i, self_j);
		if (val == 0)
			continue;

		availableVal[val - 1] = 0;
	}

	return availableVal;
};

// Check available values for a cell (in a Block)
int *sudoku::chkAvBlock(int self_i, int self_j){
	int *availableVal = new int[9];
	for (int c = 0; c < 9; c++)
		availableVal[c] = c + 1;

	int *block = getBlock(self_i, self_j);
	int *range = getBlockRange(block[0], block[1]);
	int i_min = range[0], i_max = range[1],
		j_min = range[2], j_max = range[3];

	for (int i = i_min; i <= i_max; i++)
		for (int j = j_min; j <= j_max; j++){
			if (i == self_i && j == self_j)
				continue;

			int val = getCellVal(i, j);
			if (val == 0)
				continue;

			availableVal[val - 1] = 0;
		}

	delete[] block;
	delete[] range;
	return availableVal;
};

// Update available values for a cell
int sudoku::updateCell(int i, int j, int *values){
	int *cell = getCell(i, j), change = 0;
	for (int c = 0; c < 9; c++){
		int val_new = values[c], val = cell[c];
		if (val != val_new){
			cell[c] = val_new;
			change++;
		}
	}

	return change;
};

// Check for values that occur in pairs, in the same cells
void sudoku::chkPairs(int block_i, int block_j){
	int *range = getBlockRange(block_i, block_j);
	int i_min = range[0], i_max = range[1],
		j_min = range[2], j_max = range[3];
	delete[] range;

	int cellpairs[2 * 9];
	int countpair = 0;

	for (int i = i_min; i <= i_max; i++)
		for (int j = j_min; j <= j_max; j++){
			if (getCellVal(i, j) != 0)
				continue;

			int *values = getCell(i, j);

			int count = 0, val = 0, val1 = 0, val2 = 0;
			for (int c = 0; c < 9; c++){
				val = values[c];
				if (val == 0)
					continue;

				if (val1 == 0)
					val1 = val;
				else if (val2 == 0)
					val2 = val;
				else{
					count = 3;
					break;
				}

				count++;
			};

			if (count == 2){
				cellpairs[countpair * 2 + 0] = val1;
				cellpairs[countpair * 2 + 1] = val2;
				countpair++;
			}
		};

	if (countpair <= 1)
		return;

	int pairs[2 * 9];
	int count = 0;
	int val1_1 = 0, val2_1 = 0;

	for (int c1 = 0; c1 < countpair; c1++){
		val1_1 = cellpairs[2 * c1 + 0];
		val2_1 = cellpairs[2 * c1 + 1];

		int val1_2 = 0, val2_2 = 0;
		for (int c2 = c1; c2 < countpair; c2++){
			val1_2 = cellpairs[2 * c2 + 0];
			val2_2 = cellpairs[2 * c2 + 1];

			if (val1_1 == val1_2 && val2_1 == val2_2){
				pairs[2 * count + 0] = val1_1;
				pairs[2 * count + 1] = val2_1;

				count++;
			}
		};
	};

	cout << "In block: (" << block_i << ", " << block_j << ")" << endl;
	cout << "I found " << count << " pairs:" << endl;
	for (int c = 0; c < count; c++)
		cout << " " << pairs[2 * c + 0] << ", " << pairs[2 * c + 1] << endl;

}

// Check line for possible values that occur only on one cell
int sudoku::chkLine(int self_i){
	int value[2 * 9];
	for (int c = 0; c < 9; c++){
		value[2 * c + 0] = 0;
		value[2 * c + 1] = 0;
	}

	for (int j = 0; j < 9; j++){
		int cell_val = getCellVal(self_i, j);

		if (cell_val != 0){
			value[2 * (cell_val - 1) + 0] += 2;
			value[2 * (cell_val - 1) + 1] = j;

			continue;
		};

		int *cell_values = getCell(self_i, j);

		for (int c = 0; c < 9; c++){
			int val = cell_values[c];

			if (val == 0)
				continue;

			val = 2 * (val - 1);
			value[val + 0]++;
			value[val + 1] = j;
		}
	};

	int count = 0;
	for (int c = 0; c < 9; c++){
		int val = value[2 * c + 0];
		int j = value[2 * c + 1];

		if (val == 1){
			setValue(self_i, j, c + 1);
			count++;
		}
	}

	return count;
}

// Check line for possible values that occur only on one cell
int sudoku::chkRow(int self_j){
	int value[2 * 9];
	for (int c = 0; c < 9; c++){
		value[2 * c + 0] = 0;
		value[2 * c + 1] = 0;
	}

	for (int i = 0; i < 9; i++){
		int cell_val = getCellVal(i, self_j);

		if (cell_val != 0){
			value[2 * (cell_val - 1) + 0] += 2;
			value[2 * (cell_val - 1) + 1] = i;
			continue;
		};

		int *cell_values = getCell(i, self_j);

		for (int c = 0; c < 9; c++){
			int val = cell_values[c];
			if (val == 0)
				continue;
			val = 2 * (val - 1);
			value[val + 0]++;
			value[val + 1] = i;
		}
	};

	int count = 0;
	for (int c = 0; c < 9; c++){
		int val = value[2 * c + 0];
		int i = value[2 * c + 1];

		if (val == 1){
			setValue(i, self_j, c + 1);
			count++;
		}
	}

	return count;
}

// Check block for possible values that occur only on one cell
int sudoku::chkBlock(int self_i, int self_j){
	int value[3 * 9];
	for (int c = 0; c < 9; c++){
		value[3 * c + 0] = 0;
		value[3 * c + 1] = 0;
		value[3 * c + 2] = 0;
	}

	int *block = getBlock(self_i, self_j);
	int *range = getBlockRange(block[0], block[1]);
	int i_min = range[0], i_max = range[1],
		j_min = range[2], j_max = range[3];
	delete[] block;
	delete[] range;

	for (int i = i_min; i < i_max; i++)
		for (int j = j_min; j < j_max; j++){
			int cell_val = getCellVal(i, j);

			if (cell_val != 0){
				value[3 * (cell_val - 1) + 0] += 2;
				value[3 * (cell_val - 1) + 1] = i;
				value[3 * (cell_val - 1) + 2] = j;
				continue;
			};

			int *cell_values = getCell(i, j);

			for (int c = 0; c < 9; c++){
				int val = cell_values[c];
				if (val == 0)
					continue;
				val = 3 * (val - 1);
				value[val + 0]++;
				value[val + 1] = i;
				value[val + 2] = j;
			}
		};

	int count = 0;
	for (int c = 0; c < 9; c++){
		int val = value[3 * c + 0];
		int i = value[3 * c + 1], j = value[3 * c + 2];

		if (val == 1){
			setValue(i, j, c + 1);
			count++;
		}
	}

	return count;
}

// Update all cells
int sudoku::updateCells(){
	int count = 0;
	for (int i = 0; i < 9; i++)
		for (int j = 0; j < 9; j++){
			if (getCellVal(i, j) != 0)
				continue;

			int *valuesRow, *valuesCol, *valuesBlock, values[9];

			//TODO :: Reformulate this bit to be faster
			valuesRow = chkAvRow(i, j);
			valuesCol = chkAvCol(i, j);
			valuesBlock = chkAvBlock(i, j);

			for (int c = 0; c < 9; c++){
				if (valuesRow[c] != 0 && valuesCol[c] != 0 && valuesBlock[c] != 0)
					values[c] = c + 1;
				else
					values[c] = 0;
			}

			count += updateCell(i, j, values);
		}

	return count;
};

// Last resort
void sudoku::deepSearch(){
	int *tablecopy = new int[9 * 9 * 9];
	int *copy_c = new int[9 * 9 * 9];
	int *copy_i = new int[9 * 9 * 9];
	int *copy_j = new int[9 * 9 * 9];

	copySudoku(tablecopy);
	int a = 0;
	while (!finished()){
		for (int i = 0; i < 9; i++){
			copySudoku(copy_i);
			for (int j = 0; j < 9; j++){
				if (getCellVal(i, j) != 0)
					continue;

				copySudoku(copy_j);

				int *values = getCell(i, j);
				for (int c = 0; c < 9; c++){
					if (values[c] == 0)
						continue;

					copySudoku(copy_c);
					setValue(i, j, c + 1);

					int count = 1;
					while (count > 0 && verify()){
						count = updateCells();

						for (int i = 0; i < 9; i++){
							count += chkLine(i);
							count += chkRow(i);
							/*
							for (int j = 0; j < 9; j++)
							count += chkBlock(i, j);
							*/
						};
					};

					if (!verify())
						copyToSudoku(copy_c);
				};

				if (!verify())
					copyToSudoku(copy_j);

				if (a % 10000 == 0 || a == 100){
					cout << a << endl;
					printTable();
					int b;
					cin >> b;
				};
				a++;
			}

			if (!verify())
				copyToSudoku(copy_i);

		}
	};
};


// Last resort
void sudoku::deepSearch2(){
	int *tablecopy = new int[9 * 9 * 9];
	int *copy_c = new int[9 * 9 * 9];
	int *copy_i = new int[9 * 9 * 9];
	int *copy_j = new int[9 * 9 * 9];
	int *moves = new int[(2 + 1) * 9 * 9], move = 0;
	int c = 0;

	copySudoku(tablecopy);
	int a = 0;
	while (!finished()){
		for (int i = 0; i < 9; i++){
			copySudoku(copy_i);
			for (int j = 0; j < 9; j++){
				if (getCellVal(i, j) != 0)
					continue;

				copySudoku(copy_j);

				int *values = getCell(i, j);

				for (; c < 9; c++){
					if (values[c] == 0)
						continue;

					copySudoku(copy_c);

					setValue(i, j, c + 1);

					int count = 1;
					while (count > 0 && verify()){
						count = updateCells();

						for (int i = 0; i < 9; i++){
							count += chkLine(i);
							count += chkRow(i);
							/*
							for (int j = 0; j < 9; j++)
							count += chkBlock(i, j);
							*/
						};
					};

					if (!verify())
						copyToSudoku(copy_c);
					else{
						moves[(2 + 1)*move + 0] = i;
						moves[(2 + 1)*move + 1] = j;
						moves[(2 + 1)*move + 2] = c;
						move++;
						break;
					}
				};

				if (c == 9){
					copyToSudoku(tablecopy);
					move--;
					i = moves[(2 + 1)*move + 0];
					j = moves[(2 + 1)*move + 1];
					c = moves[(2 + 1)*move + 2] - 1;
					for (int mv = 0; mv < move; mv++){
						int ii = moves[(2 + 1)*mv + 0];
						int jj = moves[(2 + 1)*mv + 1];
						int cc = moves[(2 + 1)*mv + 2];
						setValue(ii, jj, cc + 1);
					};

					continue;
				};

				c = 0;

				if (!verify())
					copyToSudoku(copy_j);

				a++;
			}

			if (!verify())
				copyToSudoku(copy_i);

		}
	};
	//cout << "Did: " << a << endl;
};

// Sort sudoku cells by number of possible values
int *sudoku::sortByPossibleValues(){
	int *ordered;
	int possible_val[2 * 9 * 9 * 7], count[7];
	int count_ij = 0, index = 0;

	for (int cc = 0; cc < 7; cc++)
		count[cc] = 0;

	for (int i = 0; i < 9; i++)
		for (int j = 0; j < 9; j++){
			count_ij = countPossible(i, j) - 2;
			if (count_ij == -1)
				continue;
			index = ((count_ij * 9 * 9 * 2) + (2 * count[count_ij]));
			possible_val[index + 0] = i;
			possible_val[index + 1] = j;

			count[count_ij]++;
		};

	int total = 0;
	for (int cc = 0; cc < 7; cc++)
		total += count[cc];

	ordered = new int[2 * total + 1];
	ordered[0] = total;

	int c = 0;
	for (int p = 0; p < 7; p++){
		if (count[p] == 0)
			continue;

		for (int p_ij = 0; p_ij < count[p]; p_ij++){
			index = (p * 9 * 9 * 2) + (p_ij * 2);
			ordered[1 + 2 * c + 0] = possible_val[index + 0];
			ordered[1 + 2 * c + 1] = possible_val[index + 1];

			c++;
		};
	};

	return ordered;
};


// Last resort
void sudoku::deepSearch3(){
	int *tablecopy = new int[9 * 9 * 9];
	int *copy_cell = new int[9 * 9 * 9];
	int *moves = new int[(1 + 2 + 1 + 1) * 9 * 9], move = 0;
	int moves_by_cell[9 * 9];
	int self_i, self_j, i, j;
	int index;

	int *order = sortByPossibleValues();
	int total = order[0];
	int *ordered = order + 1;

	bool exaustValues = false;
	bool ok = verify();

	int lastValue = 0, maxValue = 0, val = 0, *values;

	copySudoku(tablecopy);

	for (int cell = 0; cell < total; cell++){
		//cout << "I'm on cell: " << cell << endl;
		if (!ok){
			cout << "Bad result." << endl;
			break;
		}

		self_i = ordered[2 * cell + 0];
		self_j = ordered[2 * cell + 1];

		if (getCellVal(self_i, self_j) != 0){
			moves_by_cell[cell] = 0;
			continue;
		}

		values = getCell(self_i, self_j);

		// Try each value in the cell until the value satisfies the current table.
		exaustValues = false;
		for (int value = lastValue; value < 9; value++, lastValue = value){
			if (values[value] == 0){
				if (value == 8)
					exaustValues = true;
				continue;
			}
			copySudoku(copy_cell);

			for (int mv = 0; mv < 9; mv++){
				if (values[mv] != 0)
					lastValue = values[mv] - 1;
			};

			setValue(self_i, self_j, value + 1);

			ok = refresh();

			if (!ok){
				copyToSudoku(copy_cell);
				if (value == lastValue)
					exaustValues = true;
				ok = true;
				continue;
			};

			index = (1 + 2 + 1 + 1)*move;

			moves[index + 0] = cell;
			moves[index + 1] = self_i;
			moves[index + 2] = self_j;
			moves[index + 3] = value;
			moves[index + 4] = lastValue;
			move++;

			break;
		};// End value search



		if (exaustValues){
			// Search for last non-zero move and get corresponding cell 
			// and the value set different from the last possible value
			int search = move - 1;
			for (; search >= 0; search--){
				index = (1 + 2 + 1 + 1)*search;

				lastValue = moves[index + 3];
				maxValue = moves[index + 4];

				if (lastValue == maxValue){
					continue;
				}
				else{
					break;
				}
			};

			if (search < 0)
				search = 1;

			// Get to the state of this move, without applying it
			copyToSudoku(tablecopy);

			for (int mv = 0; mv < search - 1; mv++){
				index = (1 + 2 + 1 + 1)*mv;
				i = moves[index + 1];
				j = moves[index + 2];
				val = moves[index + 3];
				setValue(i, j, val + 1);
			};

			index = (1 + 2 + 1 + 1)*search;
			// Set the value of move, cell and lastValue
			if (search < 0)
				cout << "Hey" << endl;
			move = search - 1;
			cell = moves[index + 0] - 1;
			//cout << cell << " : " << moves[index + 3] << ", " << moves[index + 4]<<endl;
			lastValue = moves[index + 3] + 1;

			// Reset exaustValue boolean
			exaustValues = false;
		}
		else{
			moves_by_cell[cell] = move;
			lastValue = 0;
		}

		ok = verify();
	};

	delete[] order;

	//cout << verify()<< endl;
};
