//******************************************************************************
// Sudoku Table Class (Header)
//
// Author: Fabio R.   Date: 21/09/2013
//******************************************************************************
#ifndef SUDOKU_HPP
#define SUDOKU_HPP

#include <cstdio>
#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

class sudoku {
public:
	int *table;
	bool done;

	sudoku(){
		table = new int[9 * 9 * 9];
		done = false;

		for (int i = 0; i < 9 * 9; i++){
			int icalc = i * 9;
			for (int j = 1; j <= 9; j++)
				table[icalc + j - 1] = j;
		};
	};

	// Copy sudoku table
	void copySudoku(int *tablecopy){
		for (int i = 0; i < 9 * 9 * 9; i++)
			tablecopy[i] = table[i];
	};

	// Copy sudoku table to orginal table
	void copyToSudoku(int *tablecopy){
		for (int i = 0; i < 9 * 9 * 9; i++)
			table[i] = tablecopy[i];
	};


	////////////////////////////////////////////////////////////////////////////////
	// Sudoku table access
	//
	// Access sudoku table i,j position, all possible 9 values of it
	int* getCell(int i, int j);
	// Retrieves the value of the i,j cell
	int getCellVal(int i, int j);
	// Retrieves the cell's block i,j
	int* getBlock(int i, int j);
	// Retrieves the block range i_min, i_max, j_min, j_max
	int* getBlockRange(int i, int j);

	////////////////////////////////////////////////////////////////////////////////
	// Sudoku Validation functions
	//
	// Verify block i,j
	bool verifyBlock(int i, int j);
	// Verify all blocks
	bool verifyAllBlocks();
	// Verify line i
	bool verifyLine(int i);
	// Verify all lines
	bool verifyAllLines();
	// Verify row j
	bool verifyRow(int j);
	// Verify all rows
	bool verifyAllRows();
	// Verify a cell
	bool verifyCell(int i, int j);
	// Verify all cells
	bool verifylAllCells();
	// Verify Sudoku
	bool verify();
	// Finished?
	bool finished();

	////////////////////////////////////////////////////////////////////////////////
	// Input/Output
	//
	// Reads sudoku table
	bool parse(string game);
	// Prints sudoku table
	void printTable();
	// Prints sudoku table with guess's
	void printTableTotal();
	////////////////////////////////////////////////////////////////////////////////
	// Setting/Checking cell properties
	//
	// Checks if value occurs in line: line for the value: val (T -> occurs) 
	bool checkLine(int i, int val);
	// Checks if value occurs in line, except for the cell j (T -> occurs)
	bool checkLineOther(int i, int j_keep, int val);
	// All cells in the line, except j, won't be able to have value:val
	void cleanLine(int i, int j_keep, int val);
	// All cells in the row, except i, won't be able to have value:val
	void cleanRow(int i_keep, int j, int val);
	// All cells in the block, except (i,j), won't be able to have value:val
	void cleanBlock(int i_keep, int j_keep, int val);
	// Sets value to the cell j in line i
	void setValue(int i, int j, int val);
	// Count possible values in a cell
	int countPossible(int i, int j);

	////////////////////////////////////////////////////////////////////////////////
	// Solving functions
	//
	// Calculate available values
	bool refresh();
	// Check availabe values for a cell (in a Row)
	int *chkAvRow(int self_i, int self_j);
	// Check availabe values for a cell (in a Collumn)
	int *chkAvCol(int self_i, int self_j);
	// Check available values for a cell (in a Block)
	int *chkAvBlock(int self_i, int self_j);
	// Check for values that occur in pairs, in the same cells
	void chkPairs(int block_i, int block_j);
	// Check line for possible values that occur only on one cell
	int chkLine(int self_i);
	// Check row for possible values that occur only on one cell
	int chkRow(int self_j);
	// Check block for possible values that occur only on one cell
	int chkBlock(int self_i, int self_j);
	// Update available values for a cell
	int updateCell(int i, int j, int *values);
	// Update all cells
	int updateCells();
	// Last resort
	void deepSearch();
	// Last resort
	void deepSearch2();
	// Last resort
	void deepSearch3();
	// Sort sudoku cells by number of possible values
	int *sortByPossibleValues();
};

#endif //SUDOKU_HPP
