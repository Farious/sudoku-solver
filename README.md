# Sudoku Solver

This project began in a, somewhat, lengthy trip from Italy without the greatness of Internet and with no IDE.
I only had my Cygwin environment configured with Vim and with gcc.

## Why a sudoku solver?
Because in that same lengthy trip I got myself a Sudoku puzzle from a magazine and I wasn't being able to solve it,
so.. why not solve it by creating a software to solve it? That isn't cheating, is it?

## Today
Well I've been cleaning up my project folders and found this little precious. 
So I felt compelled to fixed some configurations with the VS IDE, pass a static code analyzer: [PVS][], tried to debug it a little to find some bugs =(.
Don't have the time, as of now, to fix it thoroughly.


[PVS]:  http://www.viva64.com/en/pvs-studio/